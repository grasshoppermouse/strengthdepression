# Low physical strength as a risk factor for depression

Hagen (2003) argued that depression is an alternative to physical aggression. That is, depression, in part, is a strategy to influence social partners with whom one is in conflict. It should be used when physical aggression is not an option. The use of physical aggression is closely tied to one's physical strength. Hence, depression should be used more often by the physically weak than by the physically strong. This could be a partial explanation for the female bias in depression because women have lower physical strength than men.

# Method

NHANES 2011-2012 measured grip strength and depression (PHQ-9). It also measured a large number of variables related to physical health.

# Predictions

1. Depression score (PHQ-9) will be negatively correlated with grip strength, controlling for age and sex.

2. Depressed status (PHQ-9 >= 10) will be negatively correlated with grip strength, controlling for age and sex.

3. Suicidal ideation (DPQ090) will be negatively correlated with grip strength, controlling for age and sex.

4. For each of the above, there will be an interaction between grip strength and sex, such that grip strength will be a stronger negative predictor for men than women (i.e., the interaction term will be significant, and the slope of grip strength vs depression will be more negative for men than women). Alternatively, after controlling for grip strength, sex will be a weaker predictor of depression (i.e., physical strength will account for much of the sex difference in depression).

5. Each of the above relationships will persist after controlling for indices of physical health (i.e., the relationship between grip strength and depression will not be entirely due to the relationship between depression and poor physical health).

6. These predictions are limited to healthy adults, aged 20-60. The reason for this age restriction is that physical strength increases dramatically during adolescence, and declines in old age.

# Preregistration

I (Edward H Hagen) declare that I have made each of these predictions prior to examining the relationship between grip strength and depression using NHANES data, or any other data, and that this statement was committed to this git repository prior to any such analyses.